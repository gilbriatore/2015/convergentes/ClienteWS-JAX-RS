package br.edu.up.ws;

import java.io.File;
import java.net.URI;
import java.security.ProtectionDomain;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;

public class JavaClienteWS {

	public static URI getURI(){
		return UriBuilder.fromUri("http://localhost:9090/javaws").build();
	}
	
	public static String getPath() {
		ProtectionDomain d = JavaClienteWS.class.getProtectionDomain();
		return d.getCodeSource().getLocation().getPath();
	}
	
	public static void main(String[] args) {

		Client cliente = ClientBuilder.newBuilder()
				  .register(MultiPartFeature.class)
				  .build();

		WebTarget ws = cliente.target(getURI());
		
		getHTML(ws);
		getHTMLPorCodigo(ws);
		getMetodoComposto(ws);
		getImagem(ws);
		getArquivoPDF(ws);
		uploadDeImagem(ws);
	}
	
	private static void uploadDeImagem(WebTarget ws) {
		
		File arquivo = new File(getPath() + "/obama.jpg");
		FormDataMultiPart multiPartes = new FormDataMultiPart();
		multiPartes.bodyPart(new FileDataBodyPart("imagem", arquivo));

		Response response = ws.path("imagem").request()
				.post(Entity.entity(multiPartes, multiPartes.getMediaType()));
		
		String status = HttpStatus.getDescription(response.getStatus());
	    System.out.println("Response uploadDeImagem(): " + status);
	}

	private static void getArquivoPDF(WebTarget ws) {
		
		Response response = ws.path("/arquivo").request()
				.accept(MediaType.APPLICATION_OCTET_STREAM)
				.get(Response.class);
		
		String status = HttpStatus.getDescription(response.getStatus());
	    System.out.println("Response getArquivoPDF(): " + status);
	}

	private static void getImagem(WebTarget ws) {
		
		Response response = ws.path("/imagem").request()
				.accept("image/jpeg")
				.get(Response.class);
		
		String status = HttpStatus.getDescription(response.getStatus());
	    System.out.println("Response getImagem(): " + status);
	}

	private static void getMetodoComposto(WebTarget ws) {
		
		String response = ws.path("/composto").request()
				.accept(MediaType.APPLICATION_JSON)
				.get(String.class).toString();
		System.out.println("Response getMetodoComposto(): " + response);	
	}

	private static void getHTMLPorCodigo(WebTarget ws) {
		
		String response = ws.path("/2").request()
				.accept(MediaType.TEXT_HTML)
				.get(String.class).toString();
		System.out.println("Response getHTMLPorCodigo(): " + response);		
	}

	private static void getHTML(WebTarget ws) {
		
		String response = ws.request()
				.accept(MediaType.TEXT_HTML)
				.get(String.class).toString();
		System.out.println("Response getHTML(): " + response);
	}
}