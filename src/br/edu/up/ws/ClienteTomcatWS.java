package br.edu.up.ws;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.ProtectionDomain;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.media.multipart.MultiPartFeature;

import br.edu.up.dominio.Endereco;
import br.edu.up.dominio.Pessoa;

public class ClienteTomcatWS {
	
	public static URI getURI(){
		return UriBuilder.fromUri("http://localhost:8080/tomcat/ws/pessoa").build();
	}

	public static void main(String[] args) {

		Client client = ClientBuilder.newBuilder()
				  .register(MultiPartFeature.class)
				  .build();

		WebTarget ws = client.target(getURI());
		
		listarPessoas(ws);
		buscarPessoaPorId(ws);
		salvarPessoa(ws);
		atualizarPessoa(ws);
		excluirPessoa(ws);
	}

	private static void excluirPessoa(WebTarget ws) {
		Response responseExcluir = ws.path("excluir").path("1")
				.request().delete(Response.class);
		String status = HttpStatus.getDescription(responseExcluir.getStatus());
	    System.out.println("Response excluir(): " + status);
	}

	private static void atualizarPessoa(WebTarget ws) {
		Endereco endereco = new Endereco(1, "Rua 2", "2222");
		byte[] foto = carregarFoto("dilma.jpg");
		Pessoa pessoa = new Pessoa(3, "Dilma", 100, foto, endereco);
		
		Response responseAtualizar = ws.path("atualizar").request()
				.put(Entity.entity(pessoa, MediaType.APPLICATION_XML),Response.class);
				
		String status = HttpStatus.getDescription(responseAtualizar.getStatus());
	    System.out.println("Response atualizar(): " + status);
	}

	private static void salvarPessoa(WebTarget ws) {
		Endereco endereco = new Endereco(1, "Rua 1", "1111");
		byte[] foto = carregarFoto("dilma.jpg");
		Pessoa pessoa = new Pessoa(3, "Dilma", 90, foto, endereco);
		
		Response responseSalvar = ws.path("salvar").request()
				.post(Entity.entity(pessoa, MediaType.APPLICATION_JSON),Response.class);
				
		String status = HttpStatus.getDescription(responseSalvar.getStatus());
	    System.out.println("Response salvar(): " + status);
	}

	private static void buscarPessoaPorId(WebTarget ws) {
		//http://localhost:8080/tomcat/ws/pessoa/buscar/1
		Response responseBuscar = ws.path("buscar").path("1").request()
				.accept(MediaType.APPLICATION_JSON).get(Response.class);
		String status = HttpStatus.getDescription(responseBuscar.getStatus());
	    System.out.println("Response buscar(): " + status);
	}

	private static void listarPessoas(WebTarget ws) {
		Response responseListar = ws.path("listar").request()
				.accept(MediaType.APPLICATION_JSON).get(Response.class);
		String status = HttpStatus.getDescription(responseListar.getStatus());
	    System.out.println("Response listar(): " + status);
	}

	public static String getPath() {
		ProtectionDomain d = ClienteTomcatWS.class.getProtectionDomain();
		return d.getCodeSource().getLocation().getPath();
	}

	private static byte[] carregarFoto(String imagem) {

		String path = getPath();
		Path pathRead = new File(path + "/" + imagem).toPath();
		byte[] foto = null;
		try {
			foto = Files.readAllBytes(pathRead);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return foto;
	}
}
