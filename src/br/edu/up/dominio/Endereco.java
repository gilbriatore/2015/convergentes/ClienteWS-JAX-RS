package br.edu.up.dominio;

public class Endereco {

	private Integer id;
	private String rua;
	private String numero;
	
	public Endereco() {
	}

	public Endereco(Integer id, String rua, String numero) {
		this.id = id; 
		this.rua = rua;
		this.numero = numero;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
}