package br.edu.up.dominio;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Pessoa {

	private Integer id;
	private String nome;
	private Integer peso;
	private byte[] foto;
	private Endereco endereco;
	
	public Pessoa() {
	}

	public Pessoa(Integer id, String nome, Integer peso, byte[] foto, Endereco endereco) {
		this.id = id;
		this.nome = nome;
		this.peso = peso;
		this.foto = foto;
		this.endereco = endereco;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getPeso() {
		return peso;
	}

	public void setPeso(Integer peso) {
		this.peso = peso;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}
}